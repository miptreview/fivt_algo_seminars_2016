echo "run 1M"
flags="-std=c++11 -Wno-deprecated"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog.exe $flags

time ./prog.exe d <<< "1000000"



echo "run  1M for generage profile"
flags="-std=c++11 -Wno-deprecated -fprofile-generate"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog_with_profile.exe $flags

time ./prog_with_profile.exe d <<< "1000000"



echo "run  1M with generated profile"
flags="-std=c++11 -Wno-deprecated -fprofile-use"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog_after_pgo.exe $flags

time ./prog_after_pgo.exe d <<< "1000000"
rm *gcda


echo "run  1M O2 for generage profile"
flags="-std=c++11 -O2 -Wno-deprecated -fprofile-generate"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog_with_profile.exe $flags

time ./prog_with_profile.exe d <<< "1000000"



echo "run  1M O2 with generated profile"
flags="-std=c++11 -Wno-deprecated -fprofile-use -O2"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog_after_pgo.exe $flags

time ./prog_after_pgo.exe d <<< "1000000"
rm *gcda


echo "run  1M O2 lto for generage profile"
flags="-std=c++11 -O2 -flto -Wno-deprecated -fprofile-generate"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog_with_profile.exe $flags

time ./prog_with_profile.exe d <<< "1000000"



echo "run  1M O2 lto with generated profile"
flags="-std=c++11 -Wno-deprecated -flto -fprofile-use -O2"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog_after_pgo.exe $flags

time ./prog_after_pgo.exe d <<< "1000000"
rm *gcda


echo "run  1M O3 lto for generage profile"
flags="-std=c++11 -O3 -flto -Wno-deprecated -fprofile-generate"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog_with_profile.exe $flags

time ./prog_with_profile.exe d <<< "1000000"



echo "run  1M O3 lto with generated profile"
flags="-std=c++11 -Wno-deprecated -flto -fprofile-use -O3"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog_after_pgo.exe $flags

time ./prog_after_pgo.exe d <<< "1000000"
rm *gcda

echo "run O2 lto 1M"
flags="-std=c++11 -Wno-deprecated -flto -O2"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog.exe $flags

time ./prog.exe d <<< "1000000"


echo "run O3 lto 1M"
flags="-std=c++11 -Wno-deprecated -flto -O3"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog.exe $flags

time ./prog.exe d <<< "1000000"


exit




g++ -c main.cpp -o main.o -std=c++11 -Wno-deprecated
g++ -c iface.cpp -o iface.o -std=c++11 -Wno-deprecated
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o -std=c++11 -Wno-deprecated
g++ -c iface_real_hard.cpp -o iface_real_hard.o -std=c++11 -Wno-deprecated
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog.exe --std=c++11 -Wno-deprecated

echo "simple run 1M"
time ./prog.exe d <<< "1000000"

g++ -c main.cpp -o main.o -std=c++11 -Wno-deprecated -O3
g++ -c iface.cpp -o iface.o -std=c++11 -Wno-deprecated -O3
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o -std=c++11 -Wno-deprecated -O3
g++ -c iface_real_hard.cpp -o iface_real_hard.o -std=c++11 -Wno-deprecated -O3
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog.exe --std=c++11 -Wno-deprecated -O3

echo "run O3 1M"
time ./prog.exe d <<< "1000000"

flags="-std=c++11 -Wno-deprecated -O3 -flto"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog.exe $flags

echo "run O3 lto 1M"
time ./prog.exe d <<< "1000000"


echo "run lto 1M"
flags="-std=c++11 -Wno-deprecated -flto"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog.exe $flags

time ./prog.exe d <<< "1000000"

echo "run O2 lto 1M"
flags="-std=c++11 -Wno-deprecated -flto -O2"

g++ -c main.cpp -o main.o $flags
g++ -c iface.cpp -o iface.o $flags
g++ -c iface_real_dummy.cpp -o iface_real_dummy.o $flags
g++ -c iface_real_hard.cpp -o iface_real_hard.o $flags
g++ main.o iface.o iface_real_dummy.o iface_real_hard.o -o prog.exe $flags

time ./prog.exe d <<< "1000000"

