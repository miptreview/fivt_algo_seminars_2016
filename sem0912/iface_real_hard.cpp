#include "iface.h"

void RealHard::F1() {
    for (int i = 0; i < 10; ++i) {
        ++c1;
        asm("");
    }
}

void RealHard::F2() {
    for (int i = 0; i < 100; ++i) {
        ++c2;
        asm("");
    }
}


void RealHard::F3() {
    for (int i = 0; i < 1000; ++i) {
        ++c3;
        asm("");
    }
}


void RealHard::F4() {
    for (int i = 0; i < 10000; ++i) {
        ++c4;
        asm("");
    }
}


void RealHard::F5() {
    for (int i = 0; i < 100000; ++i) {
        ++c5;
        asm("");
    }
}
