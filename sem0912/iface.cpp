#include "iface.h"

int Mem::count1 = 0;
int Mem::count2 = 0;
int Mem::count3 = 0;
int Mem::count4 = 0;
int Mem::count5 = 0;


void RunF1(IFace& obj) {
    obj.F1();
    ++Mem::count1;
}


void RunF2(IFace& obj) {
    obj.F2();
    ++Mem::count2;
}


void RunF3(IFace& obj) {
    obj.F3();
    ++Mem::count3;
}


void RunF4(IFace& obj) {
    obj.F1();
    ++Mem::count4;
}


void RunF5(IFace& obj) {
    obj.F5();
    ++Mem::count5;
}
