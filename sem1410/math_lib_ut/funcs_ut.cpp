#include <gtest/gtest.h>

#include "../math_lib/funcs.h"

TEST(Test1, TestGood) {
	EXPECT_EQ(add_ints(3,5),  8);
}

TEST(Test1, TestBad) {
	EXPECT_EQ(add_ints(3,5),  12);
}

TEST(Test1, TestDouble) {
	EXPECT_EQ(add_ints(3.5,5.7),  8);
}
